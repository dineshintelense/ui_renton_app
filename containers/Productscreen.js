import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { products } from "../Data";
import Products from "../components/Products";
import SearchIcon from "../containers/SearchIcon";

class Productscreen extends Component {
  render() {
    return (
      <>
        <SearchIcon />
        <View style={styles.container}>
          <Products products={products} />
        </View>
      </>
    );
  }
}
export default Productscreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
