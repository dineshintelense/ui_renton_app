import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  button,
  Image,
  Div,
  StatusBar,
  ImageBackground
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import DetailScreen from "../views/Details";
import { AntDesign } from "@expo/vector-icons";

export default class DesignScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={false} />
        <Image
          style={styles.imageali}
          source={require("../image/images.jpg")}
        ></Image>
        <Text style={styles.Text}>RENTON</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.props.navigation.navigate("Details")}
        >
          <Text style={styles.buttonText}>
            Continue
            {"                  "}
            <AntDesign
              name="arrowright"
              size={24}
              color="white"
              justifyContent="Left"
            />
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    flex: 1,
    backgroundColor: "transparent"
  },
  button: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    marginTop: 550,
    width: 300,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    margin: 35,
    position: "absolute",
    height: 50,
    top: 10
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase",
    fontSize: 16,
    textAlign: "center",
    width: 300,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    paddingLeft: 100
  },
  imageali: {
    borderRadius: 20,
    height: 300,
    width: 360,
    marginRight: 50

    // borderRadius: 200/2
  },
  Text: {
    fontWeight: "bold",
    fontSize: 70,
    alignContent: "center",
    margin: 10,
    color: "purple",
    fontFamily: "Sarpanch-SemiBold",
    textAlign: "center",
    paddingTop: 20,
    top: 200
  }
});
